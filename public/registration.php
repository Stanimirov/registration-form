<?php

header('Content-Type:application/json');

$post = !empty($_POST) ? $_POST : false;

if (empty($post)) {
    $response = [
        'message' => 'Direct access is not allowed  !!!'
    ];
    die(json_encode($response));
}

// require autoloader
require_once('../vendor/autoload.php');

$app = new App\Registration\RegistrationWebform();

//load and filter post data.
$app->load($post);

//checks if error after loading data
if (!empty($app->getFirstError())) {

    $response = [
        'type' => 'error',
        'message' => $app->getFirstError(),
        'messages' => $app->getErrors(),
        'post' => $app->getPostData()
    ];

    die(json_encode($response));
}

//start fields validation
$validate = new App\Validation\RegistrationValidation($app->getPostData());

// check empty form field/s or not meet some basic criteria like min and max length, required
if (!empty($validate->getFirstErrorField())) {

    $response = [
        'type' => 'error',
        'message' => $validate->getFirstErrorField(),
    ];

    die(json_encode($response));
}



/**
 * @todo After success validation , check for existing user into database. If user exists, return error message
 */



/**
 * @todo Create an admin report and send by email
 */


$response = [
    'type' => 'success',
    'message' => $app->getSuccessMessage(),
    'post' => $app->getPostData(),
    'form' => 'reset'
];


die(json_encode($response));

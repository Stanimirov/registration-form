/* global toastr, Ladda */

(function ($) {

    var app = {
        notify: function (params) {
            var message = typeof params === 'object' ? params.message : 'There is an error in application';
            if (typeof message === 'undefined' || !message) {
                return false;
            }
            var type = params.type !== 'undefined' ? params.type : 'error';
            switch (type) {
                case 'error':
                    toastr.error(message);
                    break;
                case 'success':
                    toastr.success(message);
                    break;
                case 'warning':
                    toastr.warning(message);
                    break;
                case 'info':
                    toastr.info(message);
                    break;
                default:
                    toastr.info(message);
                    break;
            }
            return true;
        },
        ladda: function (stop) {

            if (typeof Ladda === 'undefined' || $(".ladda-button").length < 1) {
                return false;
            }
            stop === true ? Ladda.stopAll() : Ladda.bind('.ladda-button', {});
        },
        populateSelect: function (selector) {
            if (typeof $(selector) !== 'object' || $(selector).length === 0) {
                return false;
            }
            var url = $(selector).data('url');

            if (typeof url === 'undefined') {

                return false;
            }
            var select = $(selector);

            $.getJSON(url, function (resp) {
                //select.materialSelect('destroy');
                //select.empty();
                $.each(resp, function (id, data) {
                    select.append($('<option></option>').attr('value', data.value).text(data.name));
                });
                select.materialSelect();
            });

        },
        ajaxRequest: function (form) {

            var data = $(form).serializeArray();

            var url = $(form).data('url');

            var timeout = $(form).data('timeout');

            var notify = $(form).data('notify') ? true : false;

            setTimeout(function () {
                $.ajax({
                    url: url,
                    data: data,
                    type: 'POST',
                    async: true,
                    success: function (response) {
                        if (notify === true || response.notify !== 'undefined') {
                            app.notify(response);
                        }
                        (response.reload === true) ? setTimeout(function () {
                            window.location.reload();
                        }, 1200) : "";
                        if (response.form && response.form === 'reset') {
                            $(form).trigger('reset');
                        }
                    }
                }).always(function () {
                    app.ladda(true);
                });
            }, timeout);
        }
    };

    $('body').on('click', '.autoform', function (event) {
        event.preventDefault();
        var form = $(this).parents('form');
        if ($(form).length > 0) {
            app.ajaxRequest(form);
        }
    });

    //populate selects from JSON - can have more than one select dropdown
    if ($('select[data-options="json"]').length > 0) {
        $.each($('select[data-options="json"]'), function (k, ob) {
            app.populateSelect(ob);
        });
    }
    app.ladda();
})(jQuery);
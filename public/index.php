<!DOCTYPE html>
<html>
    <head>
        <base href="/" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=yes">
        <meta name="robots" content="noindex,nofollow">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <title>Registration form</title>
        <link rel="stylesheet" type="text/css" href="resources/mdbootstrap/compiled-4.8.8.min.css" />
        <link rel="stylesheet" type="text/css" href="resources/font/material-design-icons/material-design-icons.css" />
        <link rel="stylesheet" type="text/css" href="resources/button-ladda/ladda.min.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
    </head>
    <body>
        <div class="container h-100">
            <div class="row justify-content-center mt-3">
                <h1>Registration form</h1>
            </div>
            <form class="form-horizontal registration-form" autocomplete="off" data-token="" data-token-value="" data-url="registration.php" data-timeout="700">
                <div class="card">
                    <div class="card-body py-4">   
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="md-form">
                                    <input type="text" name="first_name" class="form-control">
                                    <label>First name</label>
                                </div>    
                            </div> 
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="md-form">
                                    <input type="text" name="surname" class="form-control">
                                    <label>Surname</label>
                                </div> 
                            </div> 
                        </div> 
                        <div class="row mt-2">    
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h4>Address details</h4>
                                <select class="mdb-select md-form" name="country" 
                                        data-validate="true"
                                        data-placeholder="Choose your country" 
                                        data-options="json"  
                                        searchable="Search here.." 
                                        data-url="resources/json/countries.json">
                                    <option value="" selected >Choose your country</option>
                                </select>
                                <label class="mdb-main-label">Country</label>           
                                <div class="md-form">
                                    <input type="text" name="address" class="form-control">
                                    <label>Address</label>
                                </div>
                                <div class="md-form">
                                    <input type="text" name="postcode" class="form-control">
                                    <label>Postcode</label>
                                </div>

                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h4>Contact details</h4>
                                <div class="md-form">
                                    <i class="material-icons prefix pt-2">phone</i>
                                    <input type="number" name="phone" class="form-control">
                                    <label>Phone</label>
                                </div>
                                <div class="md-form">
                                    <i class="material-icons prefix pt-2">email</i>
                                    <input type="email" name="email" class="form-control">
                                    <label>Email</label>
                                </div>
                                <button type="button" 
                                        class="btn-info ladda-button button-ladda-spinner autoform btn waves-effect waves-light cyan" 
                                        data-style="zoom-in" 
                                        data-spinner-color="#000000">Register</button>
                            </div>
                        </div>    
                    </div> 
                </div> 
            </form>    
        </div>
        <script type="text/javascript" src="resources/mdbootstrap/compiled-4.8.8.min.js"></script>
        <script type="text/javascript" src="resources/button-ladda/spin.min.js"></script>
        <script type="text/javascript" src="resources/button-ladda/ladda.min.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
**HTML registration webform**

https://register.openapiservices.com/

**Input fields**

*  First Name
*  Surname
*  Address
*  Country
*  PostCode
*  Phone
*  Email

**Validation**


*  on submission of data, fields are validated both client and server side
*  on issue, display message to client
  
**Data**

store data into database

**Report**

Create an admin report that contains the contents of the data
<?php

namespace App\Registration;

use App\Constants\RegistrationWebformConstants;

/**
 * class RegistrationWebform
 *
 * @author Dimitar Stanimirov <stanimrov.dimitar@gmail.com>
 */
class RegistrationWebform {

    /**
     * @var array
     */
    private $post = [];

    /**
     * @var array
     */
    private $errors = [];

    public function __construct() {
        
    }

    /**
     * @param array $post POST data
     */
    public function load($post = []) {

        $data = array_map('trim', $post);

        if (empty($data)) {

            $this->setError(RegistrationWebformConstants::VALIDATION_EMPTY_FORM);

            return false;
        }

        $this->post = $data;

        return $this->post;
    }

    /**
     * @return array POST data
     */
    public function getPostData() {
        return $this->post;
    }

    /**
     * @return string Message after success form validation
     */
    public function getSuccessMessage() {

        return RegistrationWebformConstants::VALIDATION_SUCCESS;
    }

    /**
     * @return array Error messages
     */
    public function getErrors() {

        return $this->errors;
    }

    /**
     * @return string First error message
     */
    public function getFirstError() {

        return !empty($this->errors[0]) ? $this->errors[0] : false;
    }

    /**
     * Sets an Error in list with Errors
     * 
     * @param string $message
     */
    private function setError($message = '') {

        if (empty($message)) {
            return false;
        }

        $this->errors[] = $message;
    }

}

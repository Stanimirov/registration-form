<?php

namespace App\Validation;

use App\Library\Text;
use App\Constants\RegistrationWebformConstants;

/**
 * class RegistrationValidation
 *
 * @author Dimitar Stanimirov <stanimrov.dimitar@gmail.com>
 */
class RegistrationValidation {

    /**
     * @var array
     */
    private $error_fields = [];

    /**
     * @var array
     */
    private $form_fields = [];

    /**
     * @param array $fields
     */
    public function __construct($fields = []) {

        foreach ($fields as $key => $value) {

            $method = 'check' . Text::camelize($key, '_');

            if (!method_exists(get_class(), $method)) {
                continue;
            }

            $rules = $this->getFormField($key);

            if (empty($rules) || empty($rules['required'])) {
                continue;
            }

            if (empty($value) && !empty($rules['required'])) {

                $this->setErrorField($rules['title'] . ' '.RegistrationWebformConstants::VALIDATION_REQUIRED_FIELD);

                continue;
            }

            $length = $this->checkLength($value, $rules['min'], $rules['max']);

            if ($length !== true) {

                $this->setErrorField($rules['title'] . ' ' . $length);

                continue;
            }


            /**
             * call method and validate
             * @todo map key rules to class 
             */
            $this->{$method}($value, $rules);
        }
    }

    /**
     * @todo set file name and path to file into config or constant and check if json file exists
     * 
     * @return array List with form fields, including validation
     */
    public function getFormFields() {

        //prevent reading json file with form fields more than once
        if (!empty($this->form_fields)) {
            return $this->form_fields;
        }

        $fields = file_get_contents('../app/resources/forms/registration.json');

        $this->form_fields = json_decode($fields, JSON_NUMERIC_CHECK);

        return $this->form_fields;
    }

    /**
     * @return array form field validation criteria
     */
    public function getFormField($field) {

        if (empty($field)) {
            return false;
        }

        $fields = $this->getFormFields();

        return array_key_exists($field, $fields) ? $fields[$field] : false;
    }

    /**
     * @param string $email
     */
    public function checkEmail($email = '') {

        $record = 'MX';

        $exist = false;

        $emailStruct = \filter_var($email, FILTER_VALIDATE_EMAIL);

        if ($emailStruct) {

            list($user, $domain) = explode('@', $emailStruct);

            $exist = \checkdnsrr($domain, $record);
        }


        return ($exist) ?
                true :
                $this->setErrorField(RegistrationWebformConstants::VALIDATION_FIELD_EMAIL_ERROR);
    }

    /**
     * @param string $phone
     */
    public function checkPhone($phone = '') {
        
        return (preg_match('/^[0-9\+]/', $phone)) ?
                true :
                $this->setErrorField(RegistrationWebformConstants::VALIDATION_FIELD_PHONE_ERROR);
        
        
    }

    /**
     * @param string $firstName
     */
    public function checkFirstName($firstName = '') {
        
    }

    /**
     * @param string $surname
     */
    public function checkSurname($surname = '') {
        
    }

    /**
     * @param string $postcode
     */
    public function checkPostcode($postcode = '') {
        
    }

    /**
     * @param string $address
     */
    public function checkAddress($address = '') {
        
    }

    /**
     * @param string $country
     */
    public function checkCountry($country = '') {
        
    }

    /**
     * @return array List with field names and their error messages
     */
    public function getErrorFields() {
        return $this->error_fields;
    }

    /**
     * @return string filed name
     */
    public function getFirstErrorField() {

        return !empty($this->error_fields[0]) ? $this->error_fields[0] : false;
    }

    /**
     * Checks the value length
     * 
     * @param string $value
     * @param int $min
     * @param int $max
     * 
     * @return mixed string on error, bool TRUE on success
     */
    private function checkLength($value = '', $min = false, $max = false) {

        $length = strlen($value);

        $replacement = [
            '{min}' => $min,
            '{max}' => $max
        ];

        $error = true;

        if ($length < $min && $min !== false) {

            $error = strtr(RegistrationWebformConstants::VALIDATION_FIELD_MIN_ERROR, $replacement);
        } elseif ($length > $max && $max !== false) {

            return $error = strtr(RegistrationWebformConstants::VALIDATION_FIELD_MAX_ERROR, $replacement);
        }

        return $error;
    }

    /**
     * @param string $field Field name
     */
    private function setErrorField($field = false) {

        if (empty($field)) {
            return false;
        }

        $this->error_fields[] = $field;
    }

}

<?php

namespace App\Constants;

/**
 * class RegistrationWebformConstants
 *
 * @author Dimitar Stanimirov <stanimrov.dimitar@gmail.com>
 */
class RegistrationWebformConstants {

    /**
     * @var string
     */
    const VALIDATION_SUCCESS = 'Your registration was successful';

    /**
     * @var string
     */
    const VALIDATION_EMPTY_FORM = 'Please, fill all required fields';

    /**
     * @var string
     */
    const VALIDATION_REQUIRED_FIELD = 'is Required field';

    /**
     * @var string
     */
    const VALIDATION_FIELD_MAX_ERROR = 'is too long, maximum is {max} characters ({min} min).';

    /**
     * @var string
     */
    const VALIDATION_FIELD_MIN_ERROR = 'is too short, minimum is {min} characters ({max} max)';

    /**
     * @var string
     */
    const VALIDATION_FIELD_EMAIL_ERROR = 'Please, enter a valid email address';

    /**
     * @var string
     */
    const VALIDATION_FIELD_PHONE_ERROR = 'Please, enter a valid telephone number';

}

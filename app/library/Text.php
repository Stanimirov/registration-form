<?php

/**
 * class Text
 *
 * @author Dimitar Stanimirov <stanimrov.dimitar@gmail.com>
 */

namespace App\Library;

class Text {

    /**
     * Camelize text
     * 
     * @param string $string Text to camelize
     * @param string $delimeter default is dash "-"
     * 
     * @return string CamelizedText
     *      
     */
    public static function camelize($string, $delimeter = '-') {

        $parts = explode($delimeter, $string);

        $camelize = array_map('ucfirst', $parts);

        return implode('', $camelize);
    }

}
